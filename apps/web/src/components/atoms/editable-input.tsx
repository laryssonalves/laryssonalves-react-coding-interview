import { Box, Button, Typography, TypographyProps } from "@mui/material"
import { useState } from "react"

interface IEditableInputProps extends TypographyProps {
  initialValue: string
  index: number;
  onSave: (index: number, value: string) => void
}

const EditableInput: React.FC<IEditableInputProps> = ({ initialValue, index, onSave, ...typographyProps }: IEditableInputProps) => {
  const [value, setValue] = useState<string>(initialValue)
  const [editing, setEditing] = useState<boolean>(false)

  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value)
  }

  const onClickEdit = () => {
    setEditing(true)
  }

  const onClickSave = () => {
    setEditing(false)
    onSave(index, value)
  }

  const onClickCancel = () => {
    setEditing(false)
    setValue(initialValue)
  }

  if (editing) {
    return (
      <Box display="flex">
        <input
          type="text"
          onChange={onInputChange}
          value={value}
          autoFocus
        />
        <Button onClick={onClickSave}>Save</Button>
        <Button onClick={onClickCancel}>Cancel</Button>
       </Box>
    )
  }


  return (
    <Typography onClick={onClickEdit} {...typographyProps}>
      {value}
    </Typography>
  )
}

export default EditableInput