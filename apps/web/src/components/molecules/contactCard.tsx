import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import EditableInput from '@components/atoms/editable-input';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
  index: number;
  onSaveName: (index: number, value: string) => void;
  onSaveEmail: (index: number, value: string) => void;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  onSaveName,
  onSaveEmail,
  index,
  sx,
}) => {
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2} display="flex" flexDirection="column">
          <EditableInput initialValue={name} variant="subtitle1" lineHeight="1rem" onSave={onSaveName} index={index} />
          <EditableInput initialValue={email} variant="caption" color="text.secondary" onSave={onSaveEmail} index={index} />
        </Box>
      </Box>
    </Card>
  );
};
